var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var MineCounter = (function (_super) {
        __extends(MineCounter, _super);
        function MineCounter(game, x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            _super.call(this, game, x, y);
            game.add.existing(this);
        }
        MineCounter.prototype.initialize = function (x, y, value) {
            this.currentValue = value;
            this.textField = this.game.add.text(x, y, 'Mines: ' + this.currentValue, MineSwepper.Globals.defaultFont);
            this.textField.anchor.setTo(1, 0.5);
        };
        MineCounter.prototype.updateText = function (value) {
            this.textField.setText('Mines: ' + (this.currentValue - value));
        };
        return MineCounter;
    }(Phaser.Sprite));
    MineSwepper.MineCounter = MineCounter;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=MineCounter.js.map