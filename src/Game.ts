/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
/// <reference path="Boot.ts"/>
/// <reference path="Preloader.ts"/>
/// <reference path="TheGame.ts"/>


module MineSwepper
{
	export class Game extends Phaser.Game {

		constructor() {

			console.log("Game - Initializing the Game object");

			super(Globals.screenWidth, Globals.screenHeight, Phaser.AUTO, "content", null);

			this.state.add(GameStates.BOOT.toString(), Boot, false);
			this.state.add(GameStates.PRELOADER.toString(), Preloader, false);
			this.state.add(GameStates.SIZE_SELECT.toString(), SizeSelect, false);
			this.state.add(GameStates.THE_GAME.toString(), TheGame, false);

			this.state.start(GameStates.BOOT.toString());
		}

	}
}

window.onload = () => {

	let game = new MineSwepper.Game();

};