module MineSwepper {
	export class MineCounter extends Phaser.Sprite {

		textField: Phaser.Text;
		currentValue: number;

		constructor(game: Phaser.Game, x: number = 0, y: number = 0) {
			super(game, x, y);
			game.add.existing(this);
		}

		initialize(x: number, y: number, value: number) {
			this.currentValue = value;
			this.textField = this.game.add.text(x, y, 'Mines: ' + this.currentValue, Globals.defaultFont);
			this.textField.anchor.setTo(1, 0.5);
		}

		public updateText(value: number) {
			this.textField.setText('Mines: ' + (this.currentValue - value));
		}
	}
}