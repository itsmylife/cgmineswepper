/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image(MineSwepper.Globals.assets.preloader.name, MineSwepper.Globals.assets.preloader.URL);
        };
        Boot.prototype.create = function () {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            this.game.state.start(MineSwepper.GameStates.PRELOADER.toString(), true, false);
        };
        return Boot;
    }(Phaser.State));
    MineSwepper.Boot = Boot;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=Boot.js.map