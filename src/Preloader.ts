/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>

module MineSwepper {
	export class Preloader extends Phaser.State {
		preloadBar: Phaser.Sprite;

		preload() {
			this.preloadBar = this.add.sprite(this.game.width * 0.5, this.game.height * 0.5, 'preloadBar');
			this.preloadBar.anchor.set(0.5, 0.5);
			this.load.setPreloadSprite(this.preloadBar);

			this.load.spritesheet(Globals.assets.cells.name, Globals.assets.cells.URL, Globals.cellWidth, Globals.cellHeight, Globals.assets.cells.frames);
		}

		create() {
			let tween = this.add.tween(this.preloadBar).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
			tween.onComplete.add(this.startTheGame, this);
		}

		startTheGame() {
			this.game.state.start(GameStates.SIZE_SELECT.toString(), true, false);
		}
	}
}