/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
/// <reference path="Cell.ts"/>
/// <reference path="CellStates.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var Board = (function (_super) {
        __extends(Board, _super);
        function Board(game, x, y) {
            _super.call(this, game, x, y);
            this.cells = Array();
            this.mineCells = Array();
            this.flaggedCells = Array();
            this.boardCellClickedSignal = new Phaser.Signal();
            this.boardCellFlaggedSignal = new Phaser.Signal();
            this.noTileLeftSignal = new Phaser.Signal();
            game.add.existing(this);
        }
        Board.prototype.initialize = function (columns, rows, mines) {
            this.group = this.game.add.group();
            this.rows = rows;
            this.columns = columns;
            this.mines = mines;
            this.cellsLeft = (this.columns * this.rows) - this.mines;
            for (var y = 0; y < rows; y++) {
                var cellRow = [];
                var newCell = void 0;
                for (var x = 0; x < columns; x++) {
                    newCell = new MineSwepper.Cell(this.game, 0, 0);
                    newCell.initialize(x, y, this.group);
                    newCell.clickedSignal.add(this.onClickedCell, this);
                    newCell.flaggedSignal.add(this.onFlaggedCell, this);
                    cellRow.push(newCell);
                }
                this.cells.push(cellRow);
            }
            this.initializeMines();
            this.inializeAI();
        };
        Board.prototype.setPosition = function (x, y) {
            this.group.left = x;
            this.group.top = y;
        };
        Board.prototype.inializeAI = function () {
            this.ai = this.game.add.text(400, 10, "Auto Choose", MineSwepper.Globals.defaultFont);
            this.ai.anchor.set(0.5, 0.5);
            this.ai.inputEnabled = true;
            this.ai.input.useHandCursor = true;
            this.ai.events.onInputDown.add(this.onAIClicked, this);
        };
        Board.prototype.onAIClicked = function () {
            if (this.cellsLeft <= 0)
                return;
            var cell = this.getRandomCell();
            if (cell.isClicked) {
                this.onAIClicked();
                return;
            }
            cell.open();
        };
        // ------------------------------------------------------------------
        Board.prototype.getRandomCell = function () {
            var rndRow = Math.floor(Math.random() * this.rows);
            var rndColumn = Math.floor(Math.random() * this.columns);
            return this.cells[rndRow][rndColumn];
        };
        Board.prototype.initializeMines = function () {
            var cell = this.getRandomCell();
            for (var i = 0; i < this.mines; i++) {
                while (cell.currentValue == MineSwepper.CellStates.MINE) {
                    cell = this.getRandomCell();
                    this.mineCells.push(cell);
                }
                cell.currentValue = MineSwepper.CellStates.MINE;
                this.calcSurroundingCellValues(cell);
            }
        };
        Board.prototype.calcSurroundingCellValues = function (cell) {
            var targetCell;
            var surroundingCells = this.getSurroundingCells(cell);
            for (var i = 0; i < surroundingCells.length; i++) {
                targetCell = surroundingCells[i];
                if (targetCell.currentValue == MineSwepper.CellStates.MINE) {
                    continue;
                }
                targetCell.currentValue = targetCell.currentValue + 1;
            }
        };
        Board.prototype.getSurroundingCells = function (cell) {
            var cellList = [];
            var targetCell;
            var column;
            var row;
            for (var y = -1; y <= 1; y++) {
                for (var x = -1; x <= 1; x++) {
                    if (x == 0 && y == 0) {
                        continue;
                    }
                    column = cell.column + x;
                    row = cell.row + y;
                    if (row < 0 || row >= this.rows || column < 0 || column >= this.columns) {
                        continue;
                    }
                    targetCell = this.cells[row][column];
                    cellList.push(targetCell);
                }
            }
            return cellList;
        };
        // ------------------------------------------------------------------
        /*
         Signal Handlers
         */
        Board.prototype.onFlaggedCell = function (flaggedCell) {
            this.boardCellFlaggedSignal.dispatch();
            if (flaggedCell.currentState == MineSwepper.CellStates.FLAG) {
                this.flaggedCells.push(flaggedCell);
            }
            else {
                for (var i = 0; i < this.flaggedCells.length; i++) {
                    if (this.flaggedCells[i] == flaggedCell) {
                        this.flaggedCells.splice(i, 1);
                    }
                }
            }
            this.boardCellFlaggedSignal.dispatch(this.flaggedCells.length);
        };
        Board.prototype.onClickedCell = function (clickedCell) {
            this.boardCellClickedSignal.dispatch();
            var clickedCellValue = clickedCell.currentValue;
            if (clickedCellValue == MineSwepper.CellStates.ZERO) {
                this.openEmptyCells(clickedCell);
            }
            else if (clickedCellValue == MineSwepper.CellStates.MINE) {
                this.openAllCells();
            }
            this.cellsLeft--;
            if (this.cellsLeft == 0) {
                this.theEnd();
            }
        };
        Board.prototype.openEmptyCells = function (targetCell) {
            var closedCellList = [targetCell];
            var surroundingCells;
            var currentCell;
            while (closedCellList.length) {
                currentCell = closedCellList[0];
                surroundingCells = this.getSurroundingCells(currentCell);
                while (surroundingCells.length) {
                    currentCell = surroundingCells.shift();
                    if (currentCell.isClicked) {
                        continue;
                    }
                    this.cellsLeft--;
                    currentCell.clickedSignal.remove(this.onClickedCell, this);
                    currentCell.open();
                    if (currentCell.currentValue == MineSwepper.CellStates.ZERO) {
                        closedCellList.push(currentCell);
                    }
                }
                closedCellList.shift();
            }
            if (this.cellsLeft == 0) {
                this.theEnd();
            }
        };
        Board.prototype.openAllCells = function () {
            this.theEnd();
            var cell;
            for (var y = 0; y < this.rows; y++) {
                for (var x = 0; x < this.columns; x++) {
                    cell = this.cells[y][x];
                    cell.clickedSignal.remove(this.onClickedCell, this);
                    cell.flaggedSignal.remove(this.onFlaggedCell, this);
                    cell.open();
                }
            }
            this.cellsLeft = 0;
        };
        Board.prototype.theEnd = function () {
            var cell;
            for (var i = 0; i < this.mineCells.length; i++) {
                cell = this.mineCells[i];
                cell.enabled = false;
            }
            this.noTileLeftSignal.dispatch(this.flaggedCells.length == this.mines);
        };
        return Board;
    }(Phaser.Sprite));
    MineSwepper.Board = Board;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=Board.js.map