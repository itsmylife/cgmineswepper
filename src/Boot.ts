/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>

module MineSwepper {
	export class Boot extends Phaser.State {
		preload() {
			this.load.image(Globals.assets.preloader.name, Globals.assets.preloader.URL);
		}


		create() {
			this.input.maxPointers = 1;

			this.stage.disableVisibilityChange = true;

			this.game.state.start(GameStates.PRELOADER.toString(), true, false);
		}
	}
}