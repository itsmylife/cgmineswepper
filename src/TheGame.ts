/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
/// <reference path="Board.ts"/>
/// <reference path="GameTimer.ts"/>


module MineSwepper {
	export class TheGame extends Phaser.State {

		boardX: number;
		boardY: number;
		board: MineSwepper.Board;
		timer: GameTimer;
		mineCounter: MineCounter;
		endGame: EndGame;


		init() {
			this.boardX = (Globals.screenWidth - (Globals.boardWidth * Globals.cellWidth)) * 0.5;
			this.boardY = (Globals.screenHeight - (Globals.boardHeight * Globals.cellHeight)) * 0.5;
		}


		create() {
			this.initBoard();
			this.initTimer();
			this.initMineCounter();
		}

		initBoard() {
			this.board = new Board(this.game, 0, 0);
			this.board.initialize(Globals.boardWidth, Globals.boardHeight, Globals.totalMines);
			this.board.setPosition(this.boardX, this.boardY);
			this.board.boardCellClickedSignal.addOnce(this.timerStart, this);
			this.board.noTileLeftSignal.addOnce(this.timerStop, this);
			this.board.boardCellFlaggedSignal.add(this.updateMineCounter, this);
		}

		initTimer() {
			this.timer = new GameTimer(this.game);
			this.timer.initialize(this.boardX, this.boardY - 20);
		}

		initMineCounter() {
			this.mineCounter = new MineCounter(this.game);
			let xx: number = this.boardX + (Globals.boardWidth * Globals.cellWidth);
			let yy: number = this.boardY - 20;
			this.mineCounter.initialize(xx, yy, Globals.totalMines);
		}

		initEndGame(didWon: boolean) {
			this.endGame = new EndGame(this.game, 0, 0);
			this.endGame.initialize(0, 0, didWon);
		}

		timerStart() {
			this.timer.start();
		}

		timerStop(didWon: boolean) {
			this.timer.stop();
			this.initEndGame(didWon);
		}

		updateMineCounter(value: number) {
			this.mineCounter.updateText(value);
		}
	}
}