module MineSwepper {
	export class EndGame extends Phaser.Sprite {
		private winMessage: string = "You Won!!!";
		private loseMessage: string = "Lost :(";

		private message: Phaser.Text;
		private rePlayBtn: Phaser.Text;
		private selectCellSize: Phaser.Text;

		constructor(game: Phaser.Game, x: number = 0, y: number = 0) {
			super(game, x, y);
			game.add.existing(this);
		}

		public initialize(x: number, y: number, didWon: boolean) {
			this.message = this.game.add.text(x, y, didWon ? this.winMessage : this.loseMessage, Globals.defaultFont);
			this.rePlayBtn = this.game.add.text(x, y + 20, "Replay", Globals.defaultFont);
			this.selectCellSize = this.game.add.text(x, y + 40, "Different Size", Globals.defaultFont);
			//this.message.anchor.set(0.5, 0.5);
			this.rePlayBtn.inputEnabled = true;
			this.selectCellSize.inputEnabled = true;
			this.rePlayBtn.input.useHandCursor = true;
			this.selectCellSize.input.useHandCursor = true;
			this.rePlayBtn.events.onInputDown.add(this.restartGame, this);
			this.selectCellSize.events.onInputDown.add(this.selectSize, this);
		}

		private restartGame() {
			this.game.state.start(GameStates.THE_GAME.toString(), true, false);
		}

		private selectSize() {
			this.game.state.start(GameStates.SIZE_SELECT.toString(), true, false);
		}
	}
}