/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
/// <reference path="Boot.ts"/>
/// <reference path="Preloader.ts"/>
/// <reference path="TheGame.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            console.log("Game - Initializing the Game object");
            _super.call(this, MineSwepper.Globals.screenWidth, MineSwepper.Globals.screenHeight, Phaser.AUTO, "content", null);
            this.state.add(MineSwepper.GameStates.BOOT.toString(), MineSwepper.Boot, false);
            this.state.add(MineSwepper.GameStates.PRELOADER.toString(), MineSwepper.Preloader, false);
            this.state.add(MineSwepper.GameStates.SIZE_SELECT.toString(), MineSwepper.SizeSelect, false);
            this.state.add(MineSwepper.GameStates.THE_GAME.toString(), MineSwepper.TheGame, false);
            this.state.start(MineSwepper.GameStates.BOOT.toString());
        }
        return Game;
    }(Phaser.Game));
    MineSwepper.Game = Game;
})(MineSwepper || (MineSwepper = {}));
window.onload = function () {
    var game = new MineSwepper.Game();
};
//# sourceMappingURL=Game.js.map