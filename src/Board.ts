/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
/// <reference path="Cell.ts"/>
/// <reference path="CellStates.ts"/>


module MineSwepper {

	export class Board extends Phaser.Sprite {

		private group: Phaser.Group;
		private ai: Phaser.Text;

		private cells: Array<Array<Cell>> = Array<Array<Cell>>();
		private mineCells: Array<Cell> = Array<Cell>();
		private flaggedCells: Array<Cell> = Array<Cell>();

		private rows: number;
		private columns: number;
		private mines: number;
		private cellsLeft: number;

		public boardCellClickedSignal: Phaser.Signal = new Phaser.Signal();
		public boardCellFlaggedSignal: Phaser.Signal = new Phaser.Signal();
		public noTileLeftSignal: Phaser.Signal = new Phaser.Signal();


		constructor(game: Phaser.Game, x: number, y: number) {
			super(game, x, y);
			game.add.existing(this);
		}


		initialize(columns: number, rows: number, mines: number): void {
			this.group = this.game.add.group();
			this.rows = rows;
			this.columns = columns;
			this.mines = mines;
			this.cellsLeft = (this.columns * this.rows) - this.mines;

			for (let y = 0; y < rows; y++) {
				let cellRow = [];
				let newCell: Cell;

				for (let x = 0; x < columns; x++) {
					newCell = new Cell(this.game, 0, 0);
					newCell.initialize(x, y, this.group);
					newCell.clickedSignal.add(this.onClickedCell, this);
					newCell.flaggedSignal.add(this.onFlaggedCell, this);
					cellRow.push(newCell);
				}

				this.cells.push(cellRow);
			}

			this.initializeMines();
			this.inializeAI();
		}


		public setPosition(x: number, y: number) {
			this.group.left = x;
			this.group.top = y;
		}

		private inializeAI() {
			this.ai = this.game.add.text(400, 10, "Auto Choose", Globals.defaultFont);
			this.ai.anchor.set(0.5, 0.5);
			this.ai.inputEnabled = true;
			this.ai.input.useHandCursor = true;
			this.ai.events.onInputDown.add(this.onAIClicked, this);
		}

		onAIClicked() {
			if (this.cellsLeft <= 0)
				return;
			let cell: Cell = this.getRandomCell();
			if (cell.isClicked) {
				this.onAIClicked();
				return
			}
			cell.open();
		}

		// ------------------------------------------------------------------

		getRandomCell(): Cell {
			let rndRow: number = Math.floor(Math.random() * this.rows);
			let rndColumn: number = Math.floor(Math.random() * this.columns);

			return this.cells[rndRow][rndColumn];
		}


		initializeMines(): void {
			let cell: Cell = this.getRandomCell();

			for (let i: number = 0; i < this.mines; i++) {
				while (cell.currentValue == CellStates.MINE) {
					cell = this.getRandomCell();
					this.mineCells.push(cell);
				}

				cell.currentValue = CellStates.MINE;
				this.calcSurroundingCellValues(cell);
			}
		}

		calcSurroundingCellValues(cell: Cell): void {
			let targetCell: Cell;
			let surroundingCells: Array<Cell> = this.getSurroundingCells(cell);

			for (let i: number = 0; i < surroundingCells.length; i++) {
				targetCell = surroundingCells[i];
				if (targetCell.currentValue == CellStates.MINE) {
					continue;
				}
				targetCell.currentValue = targetCell.currentValue + 1;
			}
		}

		getSurroundingCells(cell: Cell): Array<Cell> {
			let cellList: Array<Cell> = [];
			let targetCell: Cell;
			let column: number;
			let row: number;

			for (let y: number = -1; y <= 1; y++) {
				for (let x: number = -1; x <= 1; x++) {
					if (x == 0 && y == 0) {
						continue;
					}

					column = cell.column + x;
					row = cell.row + y;

					if (row < 0 || row >= this.rows || column < 0 || column >= this.columns) {
						continue;
					}

					targetCell = this.cells[row][column];
					cellList.push(targetCell);
				}
			}

			return cellList;
		}

		// ------------------------------------------------------------------


		/*
		 Signal Handlers
		 */

		onFlaggedCell(flaggedCell) {
			this.boardCellFlaggedSignal.dispatch();

			if (flaggedCell.currentState == CellStates.FLAG) {
				this.flaggedCells.push(flaggedCell);
			} else {
				for (let i: number = 0; i < this.flaggedCells.length; i++) {
					if (this.flaggedCells[i] == flaggedCell) {
						this.flaggedCells.splice(i, 1);
					}
				}
			}

			this.boardCellFlaggedSignal.dispatch(this.flaggedCells.length);
		}


		onClickedCell(clickedCell: Cell) {
			this.boardCellClickedSignal.dispatch();
			let clickedCellValue: number = clickedCell.currentValue;

			if (clickedCellValue == CellStates.ZERO) {
				this.openEmptyCells(clickedCell);
			} else if (clickedCellValue == CellStates.MINE) {
				this.openAllCells();
			}

			this.cellsLeft--;

			if (this.cellsLeft == 0) {
				this.theEnd();
			}
		}


		openEmptyCells(targetCell) {
			let closedCellList: Array<Cell> = [targetCell];
			let surroundingCells: Array<Cell>;
			let currentCell: Cell;

			while (closedCellList.length) {
				currentCell = closedCellList[0];
				surroundingCells = this.getSurroundingCells(currentCell);

				while (surroundingCells.length) {
					currentCell = surroundingCells.shift();

					if (currentCell.isClicked) {
						continue;
					}

					this.cellsLeft--;
					currentCell.clickedSignal.remove(this.onClickedCell, this);
					currentCell.open();

					if (currentCell.currentValue == CellStates.ZERO) {
						closedCellList.push(currentCell);
					}
				}

				closedCellList.shift();
			}

			if (this.cellsLeft == 0) {
				this.theEnd();
			}
		}


		openAllCells() {
			this.theEnd();
			let cell: Cell;
			for (let y: number = 0; y < this.rows; y++) {
				for (let x: number = 0; x < this.columns; x++) {
					cell = this.cells[y][x];
					cell.clickedSignal.remove(this.onClickedCell, this);
					cell.flaggedSignal.remove(this.onFlaggedCell, this);
					cell.open();
				}
			}

			this.cellsLeft = 0;
		}


		theEnd() {
			let cell: Cell;
			for (let i: number = 0; i < this.mineCells.length; i++) {
				cell = this.mineCells[i];
				cell.enabled = false;
			}

			this.noTileLeftSignal.dispatch(this.flaggedCells.length == this.mines);
		}

	}
}