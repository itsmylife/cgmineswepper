module MineSwepper {


	export class Globals {
		static assets = {
			cells: {URL: 'assets/cells.png', name: 'cells', frames: 14},
			preloader: {URL: 'assets/loader.png', name: 'preloadBar'}
		};

		static defaultFont = {font: '18px Arial', fill: "#ffffff"};


		static screenWidth: number = 800;
		static screenHeight: number = 600;

		static cellWidth: number = 32;
		static cellHeight: number = 32;

		static boardWidth: number = 9;
		static boardHeight: number = 9;

		static totalMines: number = 10;


	}

	export enum GameStates
	{
		BOOT,
		PRELOADER,
		SIZE_SELECT,
		THE_GAME
	}

	export enum CellStates
	{
		ZERO = 0,
		ONE = 1,
		TWO = 2,
		THREE = 3,
		FOUR = 4,
		FIVE = 5,
		SIX = 6,
		SEVEN = 7,
		EIGHT = 8,
		DEFAULT = 9,
		FLAG = 10,
		WRONG_FLAG = 11,
		UNKNOWN = 12,
		MINE = 13
	}
}