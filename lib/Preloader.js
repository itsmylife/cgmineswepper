/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            this.preloadBar = this.add.sprite(this.game.width * 0.5, this.game.height * 0.5, 'preloadBar');
            this.preloadBar.anchor.set(0.5, 0.5);
            this.load.setPreloadSprite(this.preloadBar);
            this.load.spritesheet(MineSwepper.Globals.assets.cells.name, MineSwepper.Globals.assets.cells.URL, MineSwepper.Globals.cellWidth, MineSwepper.Globals.cellHeight, MineSwepper.Globals.assets.cells.frames);
        };
        Preloader.prototype.create = function () {
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startTheGame, this);
        };
        Preloader.prototype.startTheGame = function () {
            this.game.state.start(MineSwepper.GameStates.SIZE_SELECT.toString(), true, false);
        };
        return Preloader;
    }(Phaser.State));
    MineSwepper.Preloader = Preloader;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=Preloader.js.map