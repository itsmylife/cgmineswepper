module MineSwepper {
	export class GameTimer extends Phaser.Timer {

		counter: number = 0;
		textField: Phaser.Text;

		constructor(game: Phaser.Game) {
			super(game);
		}

		initialize(x: number, y: number) {
			this.textField = this.game.add.text(x, y, 'Time: ' + this.counter, Globals.defaultFont);
			this.textField.anchor.setTo(0, 0.5);
		}

		public start() {
			this.game.time.events.loop(Phaser.Timer.SECOND, this.updateText, this);
		}

		public stop() {
			this.game.time.events.stop();
		}


		updateText() {
			this.counter++;
			this.textField.setText('Time: ' + this.counter);
		}


	}
}