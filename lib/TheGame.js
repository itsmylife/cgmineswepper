/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
/// <reference path="Board.ts"/>
/// <reference path="GameTimer.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var TheGame = (function (_super) {
        __extends(TheGame, _super);
        function TheGame() {
            _super.apply(this, arguments);
        }
        TheGame.prototype.init = function () {
            this.boardX = (MineSwepper.Globals.screenWidth - (MineSwepper.Globals.boardWidth * MineSwepper.Globals.cellWidth)) * 0.5;
            this.boardY = (MineSwepper.Globals.screenHeight - (MineSwepper.Globals.boardHeight * MineSwepper.Globals.cellHeight)) * 0.5;
        };
        TheGame.prototype.create = function () {
            this.initBoard();
            this.initTimer();
            this.initMineCounter();
        };
        TheGame.prototype.initBoard = function () {
            this.board = new MineSwepper.Board(this.game, 0, 0);
            this.board.initialize(MineSwepper.Globals.boardWidth, MineSwepper.Globals.boardHeight, MineSwepper.Globals.totalMines);
            this.board.setPosition(this.boardX, this.boardY);
            this.board.boardCellClickedSignal.addOnce(this.timerStart, this);
            this.board.noTileLeftSignal.addOnce(this.timerStop, this);
            this.board.boardCellFlaggedSignal.add(this.updateMineCounter, this);
        };
        TheGame.prototype.initTimer = function () {
            this.timer = new MineSwepper.GameTimer(this.game);
            this.timer.initialize(this.boardX, this.boardY - 20);
        };
        TheGame.prototype.initMineCounter = function () {
            this.mineCounter = new MineSwepper.MineCounter(this.game);
            var xx = this.boardX + (MineSwepper.Globals.boardWidth * MineSwepper.Globals.cellWidth);
            var yy = this.boardY - 20;
            this.mineCounter.initialize(xx, yy, MineSwepper.Globals.totalMines);
        };
        TheGame.prototype.initEndGame = function (didWon) {
            this.endGame = new MineSwepper.EndGame(this.game, 0, 0);
            this.endGame.initialize(0, 0, didWon);
        };
        TheGame.prototype.timerStart = function () {
            this.timer.start();
        };
        TheGame.prototype.timerStop = function (didWon) {
            this.timer.stop();
            this.initEndGame(didWon);
        };
        TheGame.prototype.updateMineCounter = function (value) {
            this.mineCounter.updateText(value);
        };
        return TheGame;
    }(Phaser.State));
    MineSwepper.TheGame = TheGame;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=TheGame.js.map