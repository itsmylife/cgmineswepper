/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
/// <reference path="CellStates.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var Cell = (function (_super) {
        __extends(Cell, _super);
        function Cell(game, x, y) {
            _super.call(this, game, x, y);
            this._currentState = MineSwepper.CellStates.DEFAULT;
            this._currentValue = 0;
            this.flaggedSignal = new Phaser.Signal();
            this.clickedSignal = new Phaser.Signal();
            game.add.existing(this);
            this.altKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ALT);
        }
        Cell.prototype.initialize = function (column, row, group) {
            this._column = column;
            this._row = row;
            this.cellSprite = this.game.add.sprite(MineSwepper.Globals.cellWidth * column, MineSwepper.Globals.cellHeight * row, MineSwepper.Globals.assets.cells.name, this.currentState, group);
            this.cellSprite.inputEnabled = true;
            this.cellSprite.input.useHandCursor = true;
            this.cellSprite.events.onInputDown.add(this.onClickCell, this);
            this.altKey.onDown.add(this.altKeyDownHandler, this);
            this.altKey.onUp.add(this.altKeyUpHandler, this);
        };
        Cell.prototype.altKeyDownHandler = function () {
            this.isAltKeyPressed = true;
        };
        Cell.prototype.altKeyUpHandler = function () {
            this.isAltKeyPressed = false;
        };
        Cell.prototype.onClickCell = function () {
            if (this.isAltKeyPressed) {
                this.flagProcess();
            }
            else if (this.currentState == MineSwepper.CellStates.DEFAULT) {
                this.clickProcess();
            }
        };
        Cell.prototype.flagProcess = function () {
            switch (this.currentState) {
                case MineSwepper.CellStates.DEFAULT:
                    this.currentState = MineSwepper.CellStates.FLAG;
                    break;
                case MineSwepper.CellStates.FLAG:
                    this.currentState = MineSwepper.CellStates.UNKNOWN;
                    break;
                case MineSwepper.CellStates.UNKNOWN:
                    this.currentState = MineSwepper.CellStates.DEFAULT;
                    break;
            }
            this.flaggedSignal.dispatch(this);
            this.cellSprite.animations.frame = this.currentState;
        };
        Cell.prototype.clickProcess = function () {
            this.cellSprite.inputEnabled = false;
            this.cellSprite.animations.frame = this.currentValue;
            this.clickedSignal.dispatch(this);
        };
        Cell.prototype.open = function () {
            this.clickProcess();
        };
        Object.defineProperty(Cell.prototype, "currentValue", {
            get: function () {
                return this._currentValue;
            },
            set: function (val) {
                this._currentValue = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Cell.prototype, "currentState", {
            get: function () {
                return this._currentState;
            },
            set: function (val) {
                this._currentState = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Cell.prototype, "row", {
            get: function () {
                return this._row;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Cell.prototype, "column", {
            get: function () {
                return this._column;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Cell.prototype, "isClicked", {
            get: function () {
                return (this.cellSprite.animations.frame == this.currentValue);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Cell.prototype, "enabled", {
            get: function () {
                return this.cellSprite.inputEnabled;
            },
            set: function (val) {
                this.cellSprite.inputEnabled = val;
            },
            enumerable: true,
            configurable: true
        });
        return Cell;
    }(Phaser.Sprite));
    MineSwepper.Cell = Cell;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=Cell.js.map