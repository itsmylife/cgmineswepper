module MineSwepper {
	export class SizeSelect extends Phaser.State {

		sizes: Array<Phaser.Text>;


		create() {
			let size_9x9 = this.game.add.text(0, 0, "9x9", Globals.defaultFont);
			let size_15x10 = this.game.add.text(0, 20, "15x10", Globals.defaultFont);
			let size_17x13 = this.game.add.text(0, 40, "17x13", Globals.defaultFont);

			this.sizes = [];
			this.sizes.push(size_9x9, size_15x10, size_17x13);

			for (let i: number = 0; i < this.sizes.length; i++) {
				this.sizes[i].inputEnabled = true;
				this.sizes[i].input.useHandCursor = true;
				this.sizes[i].events.onInputDown.add(this.pickedOne, this);
			}

		}

		pickedOne(e) {
			let text: string = e.text;
			let textNums: Array<string> = text.split("x");
			Globals.boardWidth = Number(textNums[0]);
			Globals.boardHeight = Number(textNums[1]);
			let maxMines: number = Math.floor((Globals.boardWidth * Globals.boardHeight) / 3);
			let range: number = maxMines - 5;
			Globals.totalMines = Math.ceil(Math.random() * range) + 5;
			this.game.state.start(GameStates.THE_GAME.toString(), true, false);
		}
	}
}