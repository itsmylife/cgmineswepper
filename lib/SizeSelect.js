var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var SizeSelect = (function (_super) {
        __extends(SizeSelect, _super);
        function SizeSelect() {
            _super.apply(this, arguments);
        }
        SizeSelect.prototype.create = function () {
            var size_9x9 = this.game.add.text(0, 0, "9x9", MineSwepper.Globals.defaultFont);
            var size_15x10 = this.game.add.text(0, 20, "15x10", MineSwepper.Globals.defaultFont);
            var size_17x13 = this.game.add.text(0, 40, "17x13", MineSwepper.Globals.defaultFont);
            this.sizes = [];
            this.sizes.push(size_9x9, size_15x10, size_17x13);
            for (var i = 0; i < this.sizes.length; i++) {
                this.sizes[i].inputEnabled = true;
                this.sizes[i].input.useHandCursor = true;
                this.sizes[i].events.onInputDown.add(this.pickedOne, this);
            }
        };
        SizeSelect.prototype.pickedOne = function (e) {
            var text = e.text;
            var textNums = text.split("x");
            MineSwepper.Globals.boardWidth = Number(textNums[0]);
            MineSwepper.Globals.boardHeight = Number(textNums[1]);
            var maxMines = Math.floor((MineSwepper.Globals.boardWidth * MineSwepper.Globals.boardHeight) / 3);
            var range = maxMines - 5;
            MineSwepper.Globals.totalMines = Math.ceil(Math.random() * range) + 5;
            this.game.state.start(MineSwepper.GameStates.THE_GAME.toString(), true, false);
        };
        return SizeSelect;
    }(Phaser.State));
    MineSwepper.SizeSelect = SizeSelect;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=SizeSelect.js.map