var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var EndGame = (function (_super) {
        __extends(EndGame, _super);
        function EndGame(game, x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            _super.call(this, game, x, y);
            this.winMessage = "You Won!!!";
            this.loseMessage = "Lost :(";
            game.add.existing(this);
        }
        EndGame.prototype.initialize = function (x, y, didWon) {
            this.message = this.game.add.text(x, y, didWon ? this.winMessage : this.loseMessage, MineSwepper.Globals.defaultFont);
            this.rePlayBtn = this.game.add.text(x, y + 20, "Replay", MineSwepper.Globals.defaultFont);
            this.selectCellSize = this.game.add.text(x, y + 40, "Different Size", MineSwepper.Globals.defaultFont);
            //this.message.anchor.set(0.5, 0.5);
            this.rePlayBtn.inputEnabled = true;
            this.selectCellSize.inputEnabled = true;
            this.rePlayBtn.input.useHandCursor = true;
            this.selectCellSize.input.useHandCursor = true;
            this.rePlayBtn.events.onInputDown.add(this.restartGame, this);
            this.selectCellSize.events.onInputDown.add(this.selectSize, this);
        };
        EndGame.prototype.restartGame = function () {
            this.game.state.start(MineSwepper.GameStates.THE_GAME.toString(), true, false);
        };
        EndGame.prototype.selectSize = function () {
            this.game.state.start(MineSwepper.GameStates.SIZE_SELECT.toString(), true, false);
        };
        return EndGame;
    }(Phaser.Sprite));
    MineSwepper.EndGame = EndGame;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=EndGame.js.map