var MineSwepper;
(function (MineSwepper) {
    var Globals = (function () {
        function Globals() {
        }
        Globals.assets = {
            cells: { URL: 'assets/cells.png', name: 'cells', frames: 14 },
            preloader: { URL: 'assets/loader.png', name: 'preloadBar' }
        };
        Globals.defaultFont = { font: '18px Arial', fill: "#ffffff" };
        Globals.screenWidth = 800;
        Globals.screenHeight = 600;
        Globals.cellWidth = 32;
        Globals.cellHeight = 32;
        Globals.boardWidth = 9;
        Globals.boardHeight = 9;
        Globals.totalMines = 10;
        return Globals;
    }());
    MineSwepper.Globals = Globals;
    (function (GameStates) {
        GameStates[GameStates["BOOT"] = 0] = "BOOT";
        GameStates[GameStates["PRELOADER"] = 1] = "PRELOADER";
        GameStates[GameStates["SIZE_SELECT"] = 2] = "SIZE_SELECT";
        GameStates[GameStates["THE_GAME"] = 3] = "THE_GAME";
    })(MineSwepper.GameStates || (MineSwepper.GameStates = {}));
    var GameStates = MineSwepper.GameStates;
    (function (CellStates) {
        CellStates[CellStates["ZERO"] = 0] = "ZERO";
        CellStates[CellStates["ONE"] = 1] = "ONE";
        CellStates[CellStates["TWO"] = 2] = "TWO";
        CellStates[CellStates["THREE"] = 3] = "THREE";
        CellStates[CellStates["FOUR"] = 4] = "FOUR";
        CellStates[CellStates["FIVE"] = 5] = "FIVE";
        CellStates[CellStates["SIX"] = 6] = "SIX";
        CellStates[CellStates["SEVEN"] = 7] = "SEVEN";
        CellStates[CellStates["EIGHT"] = 8] = "EIGHT";
        CellStates[CellStates["DEFAULT"] = 9] = "DEFAULT";
        CellStates[CellStates["FLAG"] = 10] = "FLAG";
        CellStates[CellStates["WRONG_FLAG"] = 11] = "WRONG_FLAG";
        CellStates[CellStates["UNKNOWN"] = 12] = "UNKNOWN";
        CellStates[CellStates["MINE"] = 13] = "MINE";
    })(MineSwepper.CellStates || (MineSwepper.CellStates = {}));
    var CellStates = MineSwepper.CellStates;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=Globals.js.map