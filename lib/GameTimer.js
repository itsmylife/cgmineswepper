var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MineSwepper;
(function (MineSwepper) {
    var GameTimer = (function (_super) {
        __extends(GameTimer, _super);
        function GameTimer(game) {
            _super.call(this, game);
            this.counter = 0;
        }
        GameTimer.prototype.initialize = function (x, y) {
            this.textField = this.game.add.text(x, y, 'Time: ' + this.counter, MineSwepper.Globals.defaultFont);
            this.textField.anchor.setTo(0, 0.5);
        };
        GameTimer.prototype.start = function () {
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateText, this);
        };
        GameTimer.prototype.stop = function () {
            this.game.time.events.stop();
        };
        GameTimer.prototype.updateText = function () {
            this.counter++;
            this.textField.setText('Time: ' + this.counter);
        };
        return GameTimer;
    }(Phaser.Timer));
    MineSwepper.GameTimer = GameTimer;
})(MineSwepper || (MineSwepper = {}));
//# sourceMappingURL=GameTimer.js.map