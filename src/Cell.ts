/// <reference path="../phaser/phaser.d.ts"/>
/// <reference path="Globals.ts"/>
/// <reference path="CellStates.ts"/>

module MineSwepper {
	export class Cell extends Phaser.Sprite {

		private cellSprite: Phaser.Sprite;
		private altKey: Phaser.Key;
		private isAltKeyPressed: boolean;

		private _currentState: number = CellStates.DEFAULT;
		private _currentValue: number = 0;
		private _column: number;
		private _row: number;

		public flaggedSignal: Phaser.Signal = new Phaser.Signal();
		public clickedSignal: Phaser.Signal = new Phaser.Signal();

		constructor(game: Phaser.Game, x: number, y: number) {
			super(game, x, y);
			game.add.existing(this);
			this.altKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ALT);
		}

		initialize(column: number, row: number, group: Phaser.Group): void {
			this._column = column;
			this._row = row;
			this.cellSprite = this.game.add.sprite(
				Globals.cellWidth * column,
				Globals.cellHeight * row,
				Globals.assets.cells.name,
				this.currentState,
				group);
			this.cellSprite.inputEnabled = true;
			this.cellSprite.input.useHandCursor = true;
			this.cellSprite.events.onInputDown.add(this.onClickCell, this);
			this.altKey.onDown.add(this.altKeyDownHandler, this);
			this.altKey.onUp.add(this.altKeyUpHandler, this);
		}

		altKeyDownHandler() {
			this.isAltKeyPressed = true;
		}

		altKeyUpHandler() {
			this.isAltKeyPressed = false;
		}


		onClickCell(): void {
			if (this.isAltKeyPressed) {
				this.flagProcess();
			} else if (this.currentState == CellStates.DEFAULT) {
				this.clickProcess();
			}
		}

		flagProcess(): void {
			switch (this.currentState) {
				case CellStates.DEFAULT:
					this.currentState = CellStates.FLAG;
					break;
				case CellStates.FLAG:
					this.currentState = CellStates.UNKNOWN;
					break;
				case CellStates.UNKNOWN:
					this.currentState = CellStates.DEFAULT;
					break;
			}

			this.flaggedSignal.dispatch(this);
			this.cellSprite.animations.frame = this.currentState;
		}

		private clickProcess(): void {
			this.cellSprite.inputEnabled = false;
			this.cellSprite.animations.frame = this.currentValue;
			this.clickedSignal.dispatch(this);
		}

		public open() {
			this.clickProcess();
		}


		get currentValue(): number {
			return this._currentValue;
		}

		set currentValue(val: number) {
			this._currentValue = val;
		}


		get currentState(): number {
			return this._currentState;
		}

		set currentState(val: number) {
			this._currentState = val;
		}

		get row(): number {
			return this._row;
		}

		get column(): number {
			return this._column;
		}

		get isClicked(): boolean {
			return (this.cellSprite.animations.frame == this.currentValue)
		}


		get enabled(): boolean {
			return this.cellSprite.inputEnabled;
		}

		set enabled(val: boolean) {
			this.cellSprite.inputEnabled = val;
		}
	}
}